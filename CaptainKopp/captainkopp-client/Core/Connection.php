<?php

namespace Core;

class Connection
{

    function get_content($URL)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $URL);
        $data = curl_exec($ch);
        curl_close($ch);
        return $data;
    }
    function get_token()
    {
        $authUrl = "https://oauth2.googleapis.com/token";
        $curl = curl_init($authUrl);
        curl_setopt_array($curl, [
            CURLOPT_URL => $authUrl,
            CURLOPT_POST => TRUE,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_POSTFIELDS => [
                'code' => $_GET['code'],
                'client_id' => GOOGLE_ID,
                'client_secret' => GOOGLE_SECRET,
                'redirect_uri' => REDIRECT_URL,
                'grant_type' => 'authorization_code'
            ]
        ]);
        $http = json_decode(curl_exec($curl), true);
        curl_close($curl);
    }

    function get_info()
    {
        $link = "https://openidconnect.googleapis.com/v1/userinfo";
        $rs = curl_init($link);
        curl_setopt_array($rs, [
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HEADER => 0,
            CURLOPT_HTTPHEADER => [
                "Authorization: Bearer {$http['access_token']}"
            ]
        ]);
        $http2 = json_decode(curl_exec($rs), true);
        curl_close($rs);
    }
}
