<?php

// extraire dans un objet les fonctions
// variable get et post pour le token 
// mping config : obj valeur : donnee google , donnée sdk
// email -> mail | fistname -> 

// diix neuf / 07 23h50


// Holds the Google application Client Id, Client Secret and Redirect Url
$config_content = json_decode(file_get_contents("variable.json"), true);

// Function get access token to server
function GetAccessToken($type, $client_id, $redirect_uri, $client_secret, $code)
{
    /* Message log */
    error_log('type => ' . $type);
    error_log('client_id => ' . $client_id);
    error_log('redirect_uri => ' . $redirect_uri);
    error_log('client_secret => ' . $client_secret);
    error_log('code => ' . $code);
    /* End message log */

    $config_content = json_decode(file_get_contents("variable.json"), true);
    $url = $config_content[$type]["token_endpoint"];
    error_log('test de lurl dans le getaccess token => ' . $url);

    $curlPost = 'client_id=' . $client_id . '&redirect_uri=' . $redirect_uri . '&client_secret=' . $client_secret . '&code=' . $code . '&grant_type=authorization_code';
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $curlPost);

    $data = json_decode(curl_exec($ch), true);
    $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    if ($http_code != 200)
        throw new Exception('Error : Failed to receieve access token');

    return $data;
}
// Function exchange access token with server
function GetUserProfileInfo($type, $access_token)
{
    /* Message log */
    error_log('type => ' . $type);
    error_log('access_token => ' . $access_token);
    /* End message log */
    $config_content = json_decode(file_get_contents("variable.json"), true);
    $url = $config_content[$type]["userinfo_endpoint"];

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $access_token));
    $data = json_decode(curl_exec($ch), true);
    $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    if ($http_code != 200)
        throw new Exception('Error : Failed to get user information');

    return $data;
}

// Get type connexion with cookie ... and passes a parameter 'code' in the Redirect Url
if (isset($_GET['code'])) {
    try {

        // Get the type of acces 
        $type = $_COOKIE["service_name"];

        // Get the access token 
        $data = GetAccessToken($type, $config_content[$type]["application_identifiant"], $config_content['redirect_url'], $config_content[$type]["client_secret"], $_GET['code']);
        // Access Token
        $access_token = $data['access_token'];

        // Get user information
        $user_info = GetUserProfileInfo($type, $access_token);
        // return data simple
        // echo '<pre>', var_dump($user_info), '</pre>';
        // echo $user_info['id'] . "<br>";
        // echo $user_info['email'] . "<br>";
        // echo $user_info['name'] . "<br>";
        // echo "<img src=" . $user_info['picture'] . ">";
        
        // return data style
        echo "<!DOCTYPE html>
        <html>
        <head>
        <title>Title of the document</title>
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:300' rel='stylesheet'>
        <link href='https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css' rel='stylesheet' integrity='sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN' crossorigin='anonymous'>
        <style>
        body {
          background: whitesmoke;
          font-family: 'Open Sans', sans-serif;
        }
        .container {
          max-width: 960px;
          margin: 30px auto;
          padding: 20px;
        }
        h1 {
          font-size: 20px;
          text-align: center;
          margin: 20px 0 20px;
        }
        h1 small {
          display: block;
          font-size: 15px;
          padding-top: 8px;
          color: gray;
        }
        .avatar-upload {
          position: relative;
          max-width: 205px;
          margin: 50px auto;
        }
        .avatar-upload .avatar-edit {
          position: absolute;
          right: 12px;
          z-index: 1;
          top: 10px;
        }
        .avatar-upload .avatar-edit input {
          display: none;
        }
        .avatar-upload .avatar-edit input + label {
          display: inline-block;
          width: 34px;
          height: 34px;
          margin-bottom: 0;
          border-radius: 100%;
          background: #FFFFFF;
          border: 1px solid transparent;
          box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.12);
          cursor: pointer;
          font-weight: normal;
          transition: all 0.2s ease-in-out;
        }
        .avatar-upload .avatar-edit input + label:hover {
          background: #f1f1f1;
          border-color: #d6d6d6;
        }
        .avatar-upload .avatar-edit input + label:after {
          content: '\f040';
          font-family: 'FontAwesome';
          color: #757575;
          position: absolute;
          top: 10px;
          left: 0;
          right: 0;
          text-align: center;
          margin: auto;
        }
        .avatar-upload .avatar-preview {
          width: 192px;
          height: 192px;
          position: relative;
          border-radius: 100%;
          border: 6px solid #F8F8F8;
          box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.1);
        }
        .avatar-upload .avatar-preview > div {
          width: 100%;
          height: 100%;
          border-radius: 100%;
          background-size: cover;
          background-repeat: no-repeat;
          background-position: center;
        }
        
        
        </style>
        </head>
        
        <body>
        <div class='container'>
            <h1>".$user_info['email']." 
                <small>".$user_info['name']."</small>
            </h1>
            <div class='avatar-upload'>
                <div class='avatar-edit'>
                
                </div>
                <div class='avatar-preview'>
                    <div id='imagePreview' style='background-image: url(" . $user_info['picture'] . ");'>
                    </div>
                </div>
            </div>
        </div>
        </body>
        
        </html>
        
        ";
    } catch (Exception $e) {
        echo $e->getMessage();
        exit();
    }
}
