# OAUTH2 SOUTENANCE
![Build Status](https://img.shields.io/badge/OAUTH2-QUE%20DES%20ERREURS-red)
![](https://upload.wikimedia.org/wikipedia/fr/a/a8/Security_%28film%29_Logo.png)
> Je crois que c'est un des trucs que j'ai le moins compris cette année ..


Objectif du projet:
  - Développement d'un SDK permettant de se connecter à de multiples providers OAUTH 2.0
  - 2 providers publics obligatoires + le provider du serveur OAUTH créé en cours

### Type Oauth2

On as réalisé 3 connexion avec l'oauth2 possible ... Mais le plus avancé est le dossier "CONNECTETOI"

| Folder | Type |
| ------ | ------ |
| phpoauth2 | Guzzle (lib) |
| ConnecteToi | File Get Content |
| CaptainKopp | CURL |

