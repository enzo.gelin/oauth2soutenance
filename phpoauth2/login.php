<?php
require('config.php');
$img_profil = rand(1000, 8000);
?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <title>Login Version 1</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="files/images/icons/favicon.ico" />
    <link rel="stylesheet" type="text/css" href="files/style/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="files/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="files/style/animate/animate.css">
    <link rel="stylesheet" type="text/css" href="files/style/css-hamburgers/hamburgers.min.css">
    <link rel="stylesheet" type="text/css" href="files/style/select2/select2.min.css">
    <link rel="stylesheet" type="text/css" href="files/css/util.css">
    <link rel="stylesheet" type="text/css" href="files/css/main.css">
    <style>
        .container-login100 {
            width: 100%;
            min-height: 100vh;
            display: -webkit-box;
            display: -webkit-flex;
            display: -moz-box;
            display: -ms-flexbox;
            display: flex;
            flex-wrap: wrap;
            justify-content: center;
            align-items: center;
            padding: 15px;
            background: #c75353;
            background: -webkit-linear-gradient(-135deg, #c75353, #4158d0);
            background: -o-linear-gradient(-135deg, #c75353, #4158d0);
            background: -moz-linear-gradient(-135deg, #c75353, #4158d0);
            background: linear-gradient(-135deg, #c75353, #4158d0);
        }
    </style>
</head>

<body>
    <script>
        function statusChangeCallback(response) { // Called with the results from FB.getLoginStatus().
            console.log('statusChangeCallback');
            console.log(response); // The current login status of the person.
            if (response.status === 'connected') { // Logged into your webpage and Facebook.
                testAPI();
            } else { // Not logged into your webpage or we are unable to tell.
                document.getElementById('status').innerHTML = 'Merci de vous connecter ' +
                    'depuis cette page.';
            }
        }


        function checkLoginState() { // Called when a person is finished with the Login Button.
            FB.getLoginStatus(function(response) { // See the onlogin handler
                statusChangeCallback(response);
            });
        }


        window.fbAsyncInit = function() {
            FB.init({
                appId: '2788570841378800',
                cookie: true, // Enable cookies to allow the server to access the session.
                xfbml: true, // Parse social plugins on this webpage.
                version: 'v7.0' // Use this Graph API version for this call.
            });


            FB.getLoginStatus(function(response) { // Called after the JS SDK has been initialized.
                statusChangeCallback(response); // Returns the login status.
            });
        };


        (function(d, s, id) { // Load the SDK asynchronously
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "https://connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));


        function testAPI() { // Testing Graph API after login.  See statusChangeCallback() for when this call is made.
            console.log('Welcome!  Fetching your information.... ');
            FB.api('/me', function(response) {
                console.log('Successful login for: ' + response.name);
                document.getElementById('status').innerHTML =
                    'Merci, vous ếtes connecté ' + response.name + '!';
            });
        }
    </script>
    <div id="fb-root"></div>
    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/fr_FR/sdk.js#xfbml=1&version=v7.0&appId=2788570841378800&autoLogAppEvents=1" nonce="JsHQXFFp"></script>
    <div class="limiter">
        <div class="container-login100">
            <div class="wrap-login100">
                <div class="login100-pic js-tilt" data-tilt>
                    <img src="http://graph.facebook.com/v2.5/<?= $img_profil ?>/picture?height=400&height=400" alt="Photo de profil random de facebook" class="rondir">
                </div>
                <form class="login100-form validate-form">
                    <span class="login100-form-title">
                        Connexion Oauth2
                    </span>
                    <div class="wrap-input100 validate-input" data-validate="Un mail valide est requis : example@mail.fr">
                        <input class="input100" type="text" name="email" placeholder="Mail">
                        <span class="focus-input100"></span>
                        <span class="symbol-input100">
                            <i class="fa fa-envelope" aria-hidden="true"></i>
                        </span>
                    </div>
                    <div class="wrap-input100 validate-input" data-validate="Et ton mot de passe là ??">
                        <input class="input100" type="password" name="pass" placeholder="mot de passe">
                        <span class="focus-input100"></span>
                        <span class="symbol-input100">
                            <i class="fa fa-lock" aria-hidden="true"></i>
                        </span>
                    </div>
                    <div class="container-login100-form-btn">
                        <button class="login100-form-btn">
                            Se connecter
                        </button>
                    </div>
                    <div class="text-center p-t-12">
                        <a class="login100-form-btn red" href="https://accounts.google.com/o/oauth2/v2/auth?scope=openid%20profile%20email&access_type=online&redirect_uri=<?= urlencode('http://localhost:8585/connect.php'); ?>&response_type=code&client_id=<?= GOOGLE_ID ?>">
                            <i class="fa fa-google" aria-hidden="true"></i>&nbsp;Se connecter via Google
                        </a>
                    </div>

                    <div class="text-center p-t-12">
                        <a class="login100-form-btn red" href='https://www.facebook.com/v7.0/dialog/oauth?client_id=<?= FB_ID ?>&redirect_uri=<?= urlencode('http://localhost:8585/connect.php'); ?>&state={{st=state123abc,ds=123456789}}'>
                            <i class="fa fa-google" aria-hidden="true"></i>&nbsp;Se connecter via Facebook
                        </a>
                    </div>
                    <div class="text-center p-t-12">
                        <fb:login-button scope="public_profile,email" onlogin="checkLoginState();">
                        </fb:login-button>

                        <div id="status">
                        </div>
                    </div>
                    <div class="text-center p-t-136">
                        <small>Cette galère pour faire l'Oauth2</small>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script src="files/style/jquery/jquery-3.2.1.min.js"></script>
    <script src="files/style/bootstrap/js/popper.js"></script>
    <script src="files/style/bootstrap/js/bootstrap.min.js"></script>
    <script src="files/style/select2/select2.min.js"></script>
    <script src="files/style/tilt/tilt.jquery.min.js"></script>
    <script>
        $('.js-tilt').tilt({
            scale: 1.1
        })
    </script>
    <script src="files/js/main.js"></script>

</body>

</html>