<!DOCTYPE html>
<html>
<title>Oauth2 - Mec</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
<link rel="icon" type="image/png" href="files/images/icons/favicon.ico" />
<link rel="stylesheet" type="text/css" href="files/style/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="files/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="files/style/animate/animate.css">
<link rel="stylesheet" type="text/css" href="files/style/css-hamburgers/hamburgers.min.css">
<link rel="stylesheet" type="text/css" href="files/style/select2/select2.min.css">
<link rel="stylesheet" type="text/css" href="files/css/util.css">
<style>
    body,
    h1,
    h5 {
        font-family: "Raleway", sans-serif
    }

    body,
    html {
        height: 100%
    }

    .bgimg {
        background: #a832a4;
        background: -webkit-linear-gradient(-65deg, #a832a4, #67b8b5);
        background: -o-linear-gradient(-65deg, #a832a4, #67b8b5);
        background: -moz-linear-gradient(-65deg, #a832a4, #67b8b5);
        background: linear-gradient(-65deg, #a832a4, #67b8b5);
        min-height: 100%;
    }
</style>

<body style="overflow: hidden;">

    <div class="bgimg w3-display-container w3-text-white">
        <div class="w3-display-left w3-jumbo ">
            <img src="https://i2.wp.com/www.s2pmag.ch/wordpress/wp-content/uploads/2015/09/mario.png" alt="mariopute" class="js-tilt" data-tilt width="55%">
        </div>
        <div class="w3-display-topleft w3-container w3-xlarge">
            <p><a class="w3-button w3-black">MENU</a></p>
            <p><a href="/login.php" class="w3-button w3-black">CONNEXION</a></p>
        </div>
        <div class="w3-display-bottomleft w3-container">
            <p class="w3-xlarge">Enzo GELIN - Jérémy JUMPERTZ</p>
            <p class="w3-large">🛡️ Projet Oauth2 🛡️</p>
            <p>c'était complexe 😔</p>
        </div>
    </div>
</body>
<script src="files/style/jquery/jquery-3.2.1.min.js"></script>
<script src="files/style/bootstrap/js/popper.js"></script>
<script src="files/style/bootstrap/js/bootstrap.min.js"></script>
<script src="files/style/select2/select2.min.js"></script>
<script src="files/style/tilt/tilt.jquery.min.js"></script>
<script>
    $('.js-tilt').tilt({
        scale: 1.1
    })
</script>
<script src="files/js/main.js"></script>

</html>