<?php
session_start();
if (!isset($_SESSION['email'])) {
    header('Location: login.php');
    exit();
}
?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <title>Login Version 1</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="files/images/icons/favicon.ico" />
    <link rel="stylesheet" type="text/css" href="files/style/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="files/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="files/style/animate/animate.css">
    <link rel="stylesheet" type="text/css" href="files/style/css-hamburgers/hamburgers.min.css">
    <link rel="stylesheet" type="text/css" href="files/style/select2/select2.min.css">
    <link rel="stylesheet" type="text/css" href="files/css/util.css">
    <link rel="stylesheet" type="text/css" href="files/css/main.css">
    <style>
        .container-login100 {
            width: 100%;
            min-height: 100vh;
            display: -webkit-box;
            display: -webkit-flex;
            display: -moz-box;
            display: -ms-flexbox;
            display: flex;
            flex-wrap: wrap;
            justify-content: center;
            align-items: center;
            padding: 15px;
            background: #55a363;
            background: -webkit-linear-gradient(-135deg, #55a363, #ffff73);
            background: -o-linear-gradient(-135deg, #55a363, #ffff73);
            background: -moz-linear-gradient(-135deg, #55a363, #ffff73);
            background: linear-gradient(-135deg, #55a363, #ffff73);
        }
    </style>
</head>

<body>
    <script>
        window.fbAsyncInit = function() {
            FB.init({
                appId: '{your-app-id}',
                cookie: true,
                xfbml: true,
                version: '{api-version}'
            });

            FB.AppEvents.logPageView();

        };

        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {
                return;
            }
            js = d.createElement(s);
            js.id = id;
            js.src = "https://connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
    <div class="limiter">
        <div class="container-login100">
            <div class="wrap-login100">
                <div class="login100-pic js-tilt" data-tilt>
                    <img src="<?= $_SESSION['picture'] ?>" alt="Photo de profil google" class="rondir" referrerpolicy="no-referrer">
                </div>
                <form class="login100-form validate-form">
                    <span class="login100-form-title">
                        Bonjour, <?= $_SESSION['name'] ?>
                        <hr>
                        <p>Cette page ne devrait pas etre accessible si vous n'êtes pas connecté</p>
                    </span>
                    <div class="wrap-input100 validate-input text-center" data-validate="Un mail valide est requis : example@mail.fr">
                        <p><?= $_SESSION['email'] ?></p>
                    </div>
                    <div class="container-login100-form-btn">
                        <button class="login100-form-btn" disabled>
                            Vous êtes deja connecté
                        </button>
                    </div>
                    <div class="text-center p-t-12">
                        <a class="login100-form-btn orange" href="/reset.php">
                            Supprimer les variables $_SESSION
                        </a>
                    </div>
                    <div class="text-center p-t-136">
                        <small>Cette galère pour faire l'Oauth2</small>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script src="files/style/jquery/jquery-3.2.1.min.js"></script>
    <script src="files/style/bootstrap/js/popper.js"></script>
    <script src="files/style/bootstrap/js/bootstrap.min.js"></script>
    <script src="files/style/select2/select2.min.js"></script>
    <script src="files/style/tilt/tilt.jquery.min.js"></script>
    <script>
        $('.js-tilt').tilt({
            scale: 1.1
        })
    </script>
    <script src="files/js/main.js"></script>

</body>

</html>