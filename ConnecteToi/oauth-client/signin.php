<?php

use Core\Service;

require_once "Core/ServiceInterface.php";
require_once "Core/Service.php";

// Configuration files
$config_var = json_decode(file_get_contents('configuration.json'), true);

// Verifie si un parametre 'service' est présent dans l'url
if (isset($_GET["service"])) {
    $val_service = $_GET["service"];
    // Verifie si le fichier de config est présent
    if (isset($config_var)) {
        $service = new Service($config_var[$val_service], $val_service);
        $redirectUri = "&redirect_uri=" . urlencode($config_var['redirect_uri']);
        // Verifie si un parametre 'code' est présent dans l'url
        if ($_GET["code"] == null) {
            error_log($service->getAuthorizationEndpoint() . $redirectUri . "service=" . $service->getServiceName());
            header('Location: ' . $service->getAuthorizationEndpoint() . $redirectUri . "service=" . $service->getServiceName());
        } else {
            error_log('Le code est => ' . $_GET["code"]);
            $code = $_GET["code"];
            error_log($service->getTokenEndpoint() . $redirectUri . "service=" . $service->getServiceName() . "&code=" . $code);
            $url = $service->getTokenEndpoint() . $redirectUri . "service=" . $service->getServiceName() . "&code=" . $code;
            
            //Erreur ici en ESGI SERVICE juste (il ne veut pas acceder au token type {"token":"5f12cd18bcffd8.67318896","expireDate":{"date":"2020-07-18 11:21:12.774154","timezone_type":3,"timezone":"UTC"}} )
            $output = $service->getTokenForCode($url);
            
            if ($val_service == "github") {
                $git_data = explode("&", $output)[0];
                $git_data = explode("=", $git_data)[1];
                $json = ["access_token" => $git_data];
            } else {
                $json = json_decode($output, true);
            }

            echo $service->getUserinfoEndpoint($json["access_token"]);
        }
    } else {
        // Redirection page erreur avezc message perso
        header('Location: /erreur.php?t=1');
        exit();
    }
} else {
    // Redirection page erreur avezc message perso
    header('Location: /erreur.php?t=0');
    exit();
}
