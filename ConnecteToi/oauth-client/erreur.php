<?php 

$type_mess = $_GET['t'];
switch ($type_mess) {
    case 0:
        $message_erreur = "Aucun service n'a été trouvé dans l'url de recherche";
        break;
    case 1:
        $message_erreur = "Aucun fichier de configuration n'as était trouvé dans le système";
        break;
    case 2:
        $message_erreur = "";
        break;
}
?>
<html>

<head>
    <title>- ENcore une erreur! -</title>
    <link rel="stylesheet" href="404.css" type="text/css" media="all" />
    <style>
        body {
            background: #0000aa;
            color: #ffffff;
            font-family: courier;
            font-size: 12pt;
            text-align: center;
            margin: 100px;
        }

        blink {
            color: yellow;
        }

        .neg {
            background: #fff;
            color: #0000aa;
            padding: 2px 8px;
            font-weight: bold;
        }

        p {
            margin: 30px 100px;
            text-align: left;
        }

        a,
        a:hover {
            color: inherit;
            font: inherit;
        }

        .menu {
            text-align: center;
            margin-top: 50px;
        }
    </style>
</head>

<body>
    <span class="neg">UNE ERREUR EST SURVENUE</span>
    <p>
        <?= $message_erreur ?>
    </p>
    <p>
        En espéranr que c'est la premiere fois que vous voyez cette page<br /><br />

        * Cliquez sur MENU  pour retourner au début.
    </p>
        Cliquer sur une touche aura aucun impact<blink>_</blink>
    <div class="menu">
        <a href="/">menu</a>
    </div>

</body>

</html>