<?php


namespace Core;

class Service implements ServiceInterface
{
    private $service;
    private $appId;
    private $clientSecret;
    private $state;
    private $authorizationEndpoint;
    private $tokenEndpoint;
    private $userinfoEndpoint;

    public function __construct($json, $service)
    {
        $this->service = $service;
        $this->appId = $json["application_identifiant"];
        $this->clientSecret = $json["client_secret"];
        $this->state = $json["state"];
        $this->authorizationEndpoint = $json["authorization_endpoint"];
        $this->tokenEndpoint = $json["token_endpoint"];
        $this->userinfoEndpoint = $json["userinfo_endpoint"];
    }

    /**
     * Return the service name
     * @example google, github, ...
     */
    public function getServiceName(): string
    {
        return $this->service;
    }

    /**
     * Return the authorization endpoint
     * @example http://auth-server/auth
     */
    public function getAuthorizationEndpoint(): string
    {
        $param = "client_id=" . $this->appId . "&state=" . $this->state;
        return $this->authorizationEndpoint . $param;
    }

    /**
     * Return the access token endpoint
     * @example http://auth-server/token
     */
    public function getTokenEndpoint(): string
    {
        $param = "client_id=" . $this->appId . "&client_secret=" . $this->clientSecret;
        return $this->tokenEndpoint . $param;
    }
    /**
     * Exchange code with a token
     *
     * @param [string] $url
     * @return void
     */
    public function getTokenForCode($url)
    {
        // Curl
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 6.1; fr; rv:1.9.2.13) Gecko/20101203 Firefox/3.6.13');
        $output = curl_exec($ch);
        if (curl_error($ch)) {
            echo 'Erreur CURL : function getTokenForCode  - ' . curl_error($ch).'<br><br>';
        }
        curl_close($ch);
        return $output;
    }

    /**
     * Return the user information
     * @param $token
     * @return array
     * ]
     */
    public function getUserinfoEndpoint($token)
    {
        error_log($this->userinfoEndpoint . "&token=" . $token);
        $url = $this->userinfoEndpoint . "&token=" . $token;
        // Curl
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $token));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 6.1; fr; rv:1.9.2.13) Gecko/20101203 Firefox/3.6.13');
        $output = curl_exec($ch);
        if (curl_error($ch)) {
            echo 'Erreur CURL : function getUserinfoEndpoint  - ' . curl_error($ch) .'<br><br>';
        }
        curl_close($ch);
        return $output;
    }
}
