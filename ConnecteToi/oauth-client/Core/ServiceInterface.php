<?php

namespace Core;

interface ServiceInterface
{
    public function getServiceName(): string;
    public function getAuthorizationEndpoint(): string;
    public function getTokenEndpoint(): string;
    public function getTokenForCode($url);
    public function getUserinfoEndpoint($token);
}
